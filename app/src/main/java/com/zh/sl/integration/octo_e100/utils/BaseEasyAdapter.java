package com.zh.sl.integration.octo_e100.utils;

import android.support.annotation.Nullable;

import java.util.List;

/**
 * 单一类型 easy adapter
 * create by sl 2019/7/3
 */

public abstract class BaseEasyAdapter<T> extends BaseMultiRvAdapter<T> {

    public BaseEasyAdapter(final int mLayoutResId, @Nullable List datas) {
        super(datas);
        addItemViewDelegate(new ItemViewDelegate<T>() {
            @Override
            public int getItemViewLayoutId() {
                return mLayoutResId;
            }

            @Override
            public boolean isForViewType(T item, int position) {
                return true;
            }

            @Override
            public void convert(BaseViewHolder holder, T t, int position) {
                BaseEasyAdapter.this.convert(holder, t, position);
            }
        });
    }

    protected abstract void convert(BaseViewHolder holder, T t, int position);
}
