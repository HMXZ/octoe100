package com.zh.sl.integration.octo_e100;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.inuker.bluetooth.library.Constants;
import com.inuker.bluetooth.library.connect.response.BleWriteResponse;
import com.zh.sl.integration.octo_e100.me.ClientManager;
import com.zh.sl.integration.octo_e100.utils.BaseEasyAdapter;
import com.zh.sl.integration.octo_e100.utils.BaseMultiRvAdapter;
import com.zh.sl.integration.octo_e100.utils.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.inuker.bluetooth.library.Constants.REQUEST_FAILED;
import static com.inuker.bluetooth.library.Constants.REQUEST_SUCCESS;

public class CarControlAty extends AppCompatActivity {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private String[] sendString = new String[]{
            "0x0e 0x64 0x64 0x64 0x64 0x64 0x64",
            "0x0e 0x64 0x64 0x64 0x64 0x64 0x64",
            "0x0e 0x64 0x64 0x64 0x64 0x64 0x64",
            "0x0e 0x64 0x64 0x64 0x64 0x64 0x64",
            "0x0e 0x64 0x64 0x64 0x64 0x64 0x64",
            "0x0e 0x64 0x64 0x64 0x64 0x64 0x64",
    };

    private String[] titleString = new String[]{
            "Left-Front Sensor",
            "Front Sensor",
            "Right-Front Sensor",
            "Left-Rear Sensor",
            "Rear Sensor",
            "Right-Rear Sensor",
    };

    private int[] coverImage = new int[]{
            R.drawable.left_front,
            R.drawable.front,
            R.drawable.right_front,
            R.drawable.left_rear,
            R.drawable.rear,
            R.drawable.right_rear,
    };


    private BaseEasyAdapter<ListBean> listAdapter;

    private List<ListBean> list = new ArrayList<>();

    public static void actionAty(Context context) {
        Intent intent = new Intent(context, CarControlAty.class);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_control_aty);
        ButterKnife.bind(this);
        initData();
        initView();
    }


    private void initData() {
        for (int i = 0; i < coverImage.length; i++) {
            ListBean listBean = new ListBean();
            listBean.setImageResource(coverImage[i]);
            listBean.setSonarId(String.valueOf(i));
            listBean.setTitleString(titleString[i]);
            listBean.setSendString(sendString[i]);
            list.add(listBean);
        }
    }

    private void initView() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        listAdapter = new BaseEasyAdapter<ListBean>(R.layout.item_car_control, list) {
            @Override
            protected void convert(BaseViewHolder holder, ListBean listBean, int position) {
                holder.setImageResource(R.id.iv_cover, listBean.getImageResource());
                holder.setText(R.id.tv_name, listBean.getTitleString());
                holder.setText(R.id.tv_send_content, listBean.getSendString());
                holder.setText(R.id.tv_sonar_id, "Sonar ID: " + listBean.getSonarId());
                holder.setText(R.id.tv_distance_num, listBean.getDistanceNum() + "");
                holder.addOnSeekBarListener(R.id.seekBar);
            }
        };

        listAdapter.setOnItemSeekBarListener(new BaseMultiRvAdapter.OnItemSeekBarListener() {
            @Override
            public void onItemScroll(BaseMultiRvAdapter adapter, int progress, int position) {
                //对应每一个 item的 seekBar 监听--改变其数字
                if (Constants.STATUS_DEVICE_CONNECTED == ClientManager.getClient().getConnectStatus(ApkConfig.MAC)) {

                    //progress 代表着距离 0-500
                    //bytes 不完整 fixme
                    byte[] bytes = new byte[]{0x53, 0x54, 0x9, 0x2, 0xE, 0x64, 0x5, 0x2};
                    //存在连接对象 fixme
                    ClientManager.getClient().write(ApkConfig.MAC, ApkConfig.SERVICE_UUID, ApkConfig.CHARACTERS_UUID, bytes, new BleWriteResponse() {
                        @Override
                        public void onResponse(int code) {
                            if (code == REQUEST_SUCCESS) {
                                //写入成功
                            } else if (code == REQUEST_FAILED) {
                                //写入失败
                            }
                        }
                    });

                }
                list.get(position).setDistanceNum(progress);
                listAdapter.notifyItemChanged(position);
            }
        });
        recyclerView.setAdapter(listAdapter);
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    public class ListBean {
        String sendString;
        String titleString;
        String sonarId;
        int distanceNum;
        int imageResource;

        public int getDistanceNum() {
            return distanceNum;
        }

        public void setDistanceNum(int distanceNum) {
            this.distanceNum = distanceNum;
        }

        public String getSendString() {
            return sendString == null ? "" : sendString;
        }

        public void setSendString(String sendString) {
            this.sendString = sendString;
        }

        public String getTitleString() {
            return titleString == null ? "" : titleString;
        }

        public void setTitleString(String titleString) {
            this.titleString = titleString;
        }

        public String getSonarId() {
            return sonarId == null ? "" : sonarId;
        }

        public void setSonarId(String sonarId) {
            this.sonarId = sonarId;
        }

        public int getImageResource() {
            return imageResource;
        }

        public void setImageResource(int imageResource) {
            this.imageResource = imageResource;
        }
    }
}
