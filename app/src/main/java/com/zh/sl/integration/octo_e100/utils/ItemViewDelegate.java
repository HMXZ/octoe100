package com.zh.sl.integration.octo_e100.utils;

/**
 * 多条目类型 业务委托
 * create by sl 2019/7/4
 */
public interface ItemViewDelegate<T> {

    int getItemViewLayoutId();

    boolean isForViewType(T item, int position);

    void convert(BaseViewHolder holder, T t, int position);
}
