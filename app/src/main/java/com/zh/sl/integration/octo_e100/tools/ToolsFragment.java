package com.zh.sl.integration.octo_e100.tools;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zh.sl.integration.octo_e100.CarControlAty;
import com.zh.sl.integration.octo_e100.GameControlAty;
import com.zh.sl.integration.octo_e100.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * 工具表面碎片
 */
public class ToolsFragment extends Fragment {

    @BindView(R.id.rl_left)
    FrameLayout rlLeft;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_label)
    TextView tvLabel;
    @BindView(R.id.rl_game)
    RelativeLayout rlGame;
    @BindView(R.id.rl_sensor_left)
    FrameLayout rlSensorLeft;
    @BindView(R.id.tv_sensor_title)
    TextView tvSensorTitle;
    @BindView(R.id.tv_sensor_label)
    TextView tvSensorLabel;
    @BindView(R.id.rl_sensor)
    RelativeLayout rlSensor;
    Unbinder unbinder;

    public ToolsFragment() {

    }

    public static ToolsFragment newInstance() {
        ToolsFragment fragment = new ToolsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tools, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.rl_game, R.id.rl_sensor})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_game:
                //游戏控制
                GameControlAty.actionAty(getContext());
                break;
            case R.id.rl_sensor:
                //传感器控制
                CarControlAty.actionAty(getContext());
                break;
        }
    }
}
