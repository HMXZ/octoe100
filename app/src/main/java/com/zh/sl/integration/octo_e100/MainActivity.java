package com.zh.sl.integration.octo_e100;

import android.Manifest;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.tbruyelle.rxpermissions2.RxPermissions;
import com.zh.sl.integration.octo_e100.me.MineFragment;
import com.zh.sl.integration.octo_e100.tools.ToolsFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

/**
 * 目前首页
 */
public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.ll_fragment)
    FrameLayout llFragment;
    @BindView(R.id.rb_tools)
    RadioButton rbTools;
    @BindView(R.id.rb_me)
    RadioButton rbMe;
    @BindView(R.id.rg_tab)
    RadioGroup rgTab;

    private List<Fragment> fragmentList = new ArrayList<>();
    private int lastFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initView();
        initPermissions();
    }

    private void initView() {
        initRadioBtnView(rbTools, R.drawable.tab_tools);
        initRadioBtnView(rbMe, R.drawable.tab_me);
        fragmentList.add(ToolsFragment.newInstance());
        fragmentList.add(MineFragment.newInstance());

        lastFragment = 0;
        getSupportFragmentManager().beginTransaction().replace(R.id.ll_fragment, fragmentList.get(lastFragment))
                .show(fragmentList.get(lastFragment)).commit();

        rgTab.setOnCheckedChangeListener(this);
    }

    private void initRadioBtnView(RadioButton radioButton, @DrawableRes int resId) {
        //定义底部标签图片大小和位置
        Drawable drawable = getResources().getDrawable(resId);
        //当这个图片被绘制时，给他绑定一个矩形 ltrb规定这个矩形
        drawable.setBounds(0, 0, 60, 60);
        //设置图片在文字的哪个方向
        radioButton.setCompoundDrawables(null, drawable, null, null);
    }

    /**
     * 申请权限
     */
    private void initPermissions() {
        RxPermissions rxPermissions = new RxPermissions(MainActivity.this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                , Manifest.permission.ACCESS_FINE_LOCATION
                , Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                    }
                });
    }

    private void switchFragment(int lastFragment, int currentIndex) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(fragmentList.get(lastFragment));
        if (!fragmentList.get(currentIndex).isAdded()) {
            transaction.add(R.id.ll_fragment, fragmentList.get(currentIndex));
        }
        //这个方法避免一些异常
        transaction.show(fragmentList.get(currentIndex)).commitNowAllowingStateLoss();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_tools:
                if (lastFragment != 0) {
                    switchFragment(lastFragment, 0);
                    lastFragment = 0;
                }
                break;
            case R.id.rb_me:
                if (lastFragment != 1) {
                    switchFragment(lastFragment, 1);
                    lastFragment = 1;
                }
                break;
        }
    }

    // FIXME: 2018/9/11 ---添加
    //声明一个long类型变量：用于存放上一点击“返回键”的时刻
    private long mExitTime;

    //这里的重写主要是为了监听条目信息 的按键
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //判断用户是否点击了“返回键”
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //与上次点击返回键时刻作差
            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                //大于2000ms则认为是误操作，使用Toast进行提示
                Toast.makeText(this, "Press again to exit the program", Toast.LENGTH_SHORT).show();
                //并记录下本次点击“返回键”的时刻，以便下次进行判断
                mExitTime = System.currentTimeMillis();
            } else {
                //小于2000ms则认为是用户确实希望退出程序-调用System.exit()方法进行退出
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
