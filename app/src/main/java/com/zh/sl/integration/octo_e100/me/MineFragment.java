package com.zh.sl.integration.octo_e100.me;


import android.Manifest;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.inuker.bluetooth.library.beacon.Beacon;
import com.inuker.bluetooth.library.connect.listener.BluetoothStateListener;
import com.inuker.bluetooth.library.connect.options.BleConnectOptions;
import com.inuker.bluetooth.library.connect.response.BleConnectResponse;
import com.inuker.bluetooth.library.model.BleGattProfile;
import com.inuker.bluetooth.library.search.SearchRequest;
import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.search.response.SearchResponse;
import com.inuker.bluetooth.library.utils.BluetoothLog;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.zh.sl.integration.octo_e100.ApkConfig;
import com.zh.sl.integration.octo_e100.MainActivity;
import com.zh.sl.integration.octo_e100.R;
import com.zh.sl.integration.octo_e100.utils.BaseEasyAdapter;
import com.zh.sl.integration.octo_e100.utils.BaseMultiRvAdapter;
import com.zh.sl.integration.octo_e100.utils.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.functions.Consumer;

import static com.inuker.bluetooth.library.Code.REQUEST_SUCCESS;
import static com.inuker.bluetooth.library.Constants.REQUEST_FAILED;

/**
 * 我的  蓝牙github https://github.com/dingjikerbo/Android-BluetoothKit
 */
public class MineFragment extends Fragment {


    @BindView(R.id.tv_search)
    TextView tvSearch;
    @BindView(R.id.tv_status)
    TextView tvSearchStatus;
    @BindView(R.id.recycleView)
    RecyclerView recycleView;
    Unbinder unbinder;
    private List<SearchResult> mDevices = new ArrayList<>();
    private BaseEasyAdapter<SearchResult> deviceAdapter;

    private MyBluetoothStateListener myBluetoothStateListener;
    private BleConnectOptions options;

    public static MineFragment newInstance() {
        MineFragment fragment = new MineFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mine, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        myBluetoothStateListener = new MyBluetoothStateListener();
        ClientManager.getClient().registerBluetoothStateListener(myBluetoothStateListener);

        recycleView.setLayoutManager(new LinearLayoutManager(getContext()));
        deviceAdapter = new BaseEasyAdapter<SearchResult>(R.layout.item_device_list, mDevices) {
            @Override
            protected void convert(BaseViewHolder holder, SearchResult searchResult, int position) {

                holder.setText(R.id.name, searchResult.getName());
                holder.setText(R.id.mac, searchResult.getAddress());
                holder.setText(R.id.rssi, String.format("Rssi: %d", searchResult.rssi));
                Beacon beacon = new Beacon(searchResult.scanRecord);
                holder.setText(R.id.adv, beacon.toString());
            }
        };
        recycleView.setAdapter(deviceAdapter);
        deviceAdapter.setOnItemClickListener(new BaseMultiRvAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseMultiRvAdapter adapter, View view, final int position) {
                //点击连结
                ClientManager.getClient().connect(mDevices.get(position).getAddress(), options, new BleConnectResponse() {
                    @Override
                    public void onResponse(int code, BleGattProfile profile) {
                        if (code == REQUEST_SUCCESS) {

                            ApkConfig.MAC = mDevices.get(position).getAddress();
                            ApkConfig.SERVICE_UUID = profile.getServices().get(0).getUUID();
                            ApkConfig.CHARACTERS_UUID = profile.getServices().get(0).getCharacters().get(0).getUuid();

                            Toast.makeText(getContext(), "连接成功", Toast.LENGTH_SHORT).show();
                        }
                        if (code == REQUEST_FAILED) {
                            Toast.makeText(getContext(), "连接失败", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        //连接配置
        options = new BleConnectOptions.Builder()
                .setConnectRetry(3)
                .setConnectTimeout(20000)
                .setServiceDiscoverRetry(3)
                .setServiceDiscoverTimeout(10000)
                .build();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        ClientManager.getClient().unregisterBluetoothStateListener(myBluetoothStateListener);
    }

    @OnClick(R.id.ll_search)
    public void onViewClicked() {
        if (ClientManager.getClient().isBluetoothOpened()) {
            initPermissions();
        } else {
            Toast.makeText(getContext(), "请打开蓝牙", Toast.LENGTH_LONG).show();
            try {
                ClientManager.getClient().openBluetooth();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 申请权限
     */
    private void initPermissions() {
        RxPermissions rxPermissions = new RxPermissions(getActivity());
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE
                , Manifest.permission.ACCESS_FINE_LOCATION
                , Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        searchDevice();
                    }

                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                    }
                });
    }

    /**
     * 寻找设备
     */
    private void searchDevice() {
        mDevices.clear();
        SearchRequest request = new SearchRequest.Builder()
                .searchBluetoothLeDevice(3000, 3)   // 先扫BLE设备3次，每次3s
                .searchBluetoothClassicDevice(5000) // 再扫经典蓝牙5s
                .searchBluetoothLeDevice(2000)// 再扫BLE设备2s.build();
                .build();
        ClientManager.getClient().search(request, mSearchResponse);
    }

    private final SearchResponse mSearchResponse = new SearchResponse() {
        @Override
        public void onSearchStarted() {
            BluetoothLog.w("MainActivity.onSearchStarted");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvSearchStatus.setText("Searching...");
                }
            });
            mDevices.clear();
        }

        @Override
        public void onDeviceFounded(SearchResult device) {
//            BluetoothLog.w("MainActivity.onDeviceFounded " + device.device.getAddress());
            if (!mDevices.contains(device)) {
                mDevices.add(device);
                deviceAdapter.notifyDataSetChanged();
//                Beacon beacon = new Beacon(device.scanRecord);
//                BluetoothLog.v(String.format("beacon for %s\n%s", device.getAddress(), beacon.toString()));
//                BeaconItem beaconItem = null;
//                BeaconParser beaconParser = new BeaconParser(beaconItem);
//                int firstByte = beaconParser.readByte(); // 读取第1个字节
//                int secondByte = beaconParser.readByte(); // 读取第2个字节
//                int productId = beaconParser.readShort(); // 读取第3,4个字节
//                boolean bit1 = beaconParser.getBit(firstByte, 0); // 获取第1字节的第1bit
//                boolean bit2 = beaconParser.getBit(firstByte, 1); // 获取第1字节的第2bit
//                beaconParser.setPosition(0); // 将读取起点设置到第1字节处
            }
        }

        @Override
        public void onSearchStopped() {
            BluetoothLog.w("MainActivity.onSearchStopped");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvSearchStatus.setText("onSearchStopped");
                }
            });

        }

        @Override
        public void onSearchCanceled() {
            BluetoothLog.w("MainActivity.onSearchCanceled");
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvSearchStatus.setText("onSearchCanceled");
                }
            });
        }
    };


    private class MyBluetoothStateListener extends BluetoothStateListener {

        @Override
        public void onBluetoothStateChanged(boolean openOrClosed) {
            if (openOrClosed) {
                //蓝牙打开
            }
        }
    }
}
