package com.zh.sl.integration.octo_e100;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.inuker.bluetooth.library.Constants;
import com.inuker.bluetooth.library.connect.response.BleReadResponse;
import com.inuker.bluetooth.library.connect.response.BleWriteResponse;
import com.zh.sl.integration.octo_e100.me.ClientManager;
import com.zh.sl.integration.octo_e100.utils.Utils;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.controlwear.virtual.joystick.android.JoystickView;

import static com.inuker.bluetooth.library.Constants.REQUEST_FAILED;
import static com.inuker.bluetooth.library.Constants.REQUEST_SUCCESS;

/**
 * 手柄控制器
 * 遥杆git 地址:https://github.com/controlwear/virtual-joystick-android
 */
public class GameControlAty extends AppCompatActivity {

    @BindView(R.id.tv_analogue)
    TextView tvAnalogue;
    @BindView(R.id.tv_analogueX)
    TextView tvAnalogueX;
    @BindView(R.id.tv_analogueY)
    TextView tvAnalogueY;
    @BindView(R.id.tv_package)
    TextView tvPackage;
    @BindView(R.id.tv_package_sent)
    TextView tvPackageSent;
    @BindView(R.id.tv_a)
    TextView tvA;
    @BindView(R.id.tv_b)
    TextView tvB;
    @BindView(R.id.joystickView)
    JoystickView joystickView;
    private int controlStatus = 1;// A:1 B:2 //默认是1

    private int oldAnale, oldStrength;


    public static void actionAty(Context context) {
        Intent intent = new Intent(context, GameControlAty.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_control);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        //遥杆监听
        joystickView.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {

                if (oldAnale != angle || oldStrength != strength) {
                    oldAnale = angle;
                    oldStrength = strength;
                    // 原点 原始坐标 50 50;
                    float xfloat = (joystickView.getNormalizedX() - 50) / 50;
                    float yfloat = (joystickView.getNormalizedY() - 50) / 50;
                    byte[] xByte = Utils.float2byte(xfloat);
                    byte[] yByte = Utils.float2byte(yfloat);

                    //此时动作发生改变
                    if (Constants.STATUS_DEVICE_CONNECTED == ClientManager.getClient().getConnectStatus(ApkConfig.MAC)) {
                        byte[] bytes = new byte[]{0x53, 0x54, 0x7, 0x1, 0xE, 0x64, 0x3, 0x2, 0xA,
                                xByte[0], xByte[1], xByte[2], xByte[3],
                                yByte[0], yByte[1], yByte[2], yByte[3]};
                        if (controlStatus == 1) {
                            //A
                            bytes[8] = 0xA;
                        } else {
                            bytes[8] = 0xB;
                        }
                        tvPackageSent.setText(bytes.toString());
                        //存在连接对象 fixme
                        ClientManager.getClient().write(ApkConfig.MAC, ApkConfig.SERVICE_UUID, ApkConfig.CHARACTERS_UUID, bytes, new BleWriteResponse() {
                            @Override
                            public void onResponse(int code) {
                                if (code == REQUEST_SUCCESS) {
                                    //写入成功
                                } else if (code == REQUEST_FAILED) {
                                    //写入失败
                                }
                            }
                        });

                    }

                }

                tvAnalogueX.setText(joystickView.getNormalizedX() + "");
                tvAnalogueY.setText(joystickView.getNormalizedY() + "");

                // do whatever you want
                Log.e("angle::", angle + "");
                Log.e("strength::", strength + "");
            }
        }, 50); //50ms 刷新频率
    }

    @OnClick({R.id.tv_a, R.id.tv_b})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_a:
                controlStatus = 1;
                changeTextColor();
                break;
            case R.id.tv_b:
                controlStatus = 2;
                changeTextColor();
                break;
        }
    }

    private void changeTextColor() {
        tvA.setBackgroundResource(R.drawable.shape_choice_oval);
        tvB.setBackgroundResource(R.drawable.shape_choice_oval);
        switch (controlStatus) {
            case 1:
                tvA.setBackgroundResource(R.drawable.shape_choice_oval_per);
                break;
            case 2:
                tvB.setBackgroundResource(R.drawable.shape_choice_oval_per);
                break;
        }
    }
}
