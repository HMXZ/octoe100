package com.zh.sl.integration.octo_e100.utils;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * create by sl 2019/7/4
 */
public class BaseMultiRvAdapter<T> extends RecyclerView.Adapter<BaseViewHolder> {
    private List<T> datas;

    protected ItemViewDelegateManager mItemViewDelegateManager;
    protected LayoutInflater mLayoutInflater;
    protected Context mContext;
    private OnItemClickListener mOnItemClickListener;
    private OnItemChildClickListener mOnItemChildClickListener;
    private OnItemSeekBarListener mOnItemSeekBarListener;

    public BaseMultiRvAdapter(@Nullable List<T> datas) {
        this.datas = datas == null ? new ArrayList<T>() : datas;
        mItemViewDelegateManager = new ItemViewDelegateManager();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        BaseViewHolder baseViewHolder = null;
        //学到一手
        this.mContext = parent.getContext();
        this.mLayoutInflater = LayoutInflater.from(mContext);
        ItemViewDelegate itemViewDelegate = mItemViewDelegateManager.getItemViewDelegate(viewType);
        int layoutId = itemViewDelegate.getItemViewLayoutId();
        baseViewHolder = onCreateDefViewHolder(layoutId, parent, viewType);
        bindViewClickListener(baseViewHolder);
        baseViewHolder.setAdapter(this);
        return baseViewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int position) {
        convert(baseViewHolder, datas.get(position));
    }


    @Override
    public int getItemCount() {
        return datas != null ? datas.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (!useItemViewDelegateManager()) {
            return super.getItemViewType(position);
        }
        //返回多种类型的下标
        return mItemViewDelegateManager.getItemViewType(datas.get(position), position);
    }

    protected boolean useItemViewDelegateManager() {
        return mItemViewDelegateManager.getItemViewDelegateCount() > 0;
    }

    public BaseMultiRvAdapter addItemViewDelegate(ItemViewDelegate<T> itemViewDelegate) {
        mItemViewDelegateManager.addDelegate(itemViewDelegate);
        return this;
    }

    public void convert(BaseViewHolder holder, T t) {
        mItemViewDelegateManager.convert(holder, t, holder.getAdapterPosition());
    }

    protected BaseViewHolder onCreateDefViewHolder(@LayoutRes int layoutResId, ViewGroup parent, int viewType) {
        return new BaseViewHolder(getItemView(layoutResId, parent));
    }

    protected View getItemView(@LayoutRes int layoutResId, ViewGroup parent) {
        return mLayoutInflater.inflate(layoutResId, parent, false);
    }


    private void bindViewClickListener(final BaseViewHolder baseViewHolder) {
        if (baseViewHolder == null) {
            return;
        }
        final View view = baseViewHolder.itemView;
        if (view == null) {
            return;
        }
        if (getOnItemClickListener() != null) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setOnItemClick(v, baseViewHolder.getLayoutPosition());
                }
            });
        }
    }

    public void setOnItemClick(View v, int position) {
        getOnItemClickListener().onItemClick(BaseMultiRvAdapter.this, v, position);
    }


    public void setOnItemClickListener(@Nullable OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }


    public void setOnItemChildClickListener(OnItemChildClickListener listener) {
        mOnItemChildClickListener = listener;
    }

    public void setOnItemSeekBarListener(@Nullable OnItemSeekBarListener listener) {
        mOnItemSeekBarListener = listener;
    }

    @Nullable
    public final OnItemClickListener getOnItemClickListener() {
        return mOnItemClickListener;
    }

    @Nullable
    public final OnItemChildClickListener getOnItemChildClickListener() {
        return mOnItemChildClickListener;
    }

    public OnItemSeekBarListener getmOnItemSeekBarListener() {
        return mOnItemSeekBarListener;
    }

    public interface OnItemChildClickListener {
        void onItemChildClick(BaseMultiRvAdapter adapter, View view, int position);
    }

    public interface OnItemClickListener {
        void onItemClick(BaseMultiRvAdapter adapter, View view, int position);
    }

    public interface OnItemSeekBarListener {
        void onItemScroll(BaseMultiRvAdapter adapter, int progress, int position);
    }
}
